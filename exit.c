/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:28:09 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:28:14 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		clear_all(t_all *data)
{
	int i;

	i = -1;
	while (++i < D->xy[1])
		free(D->map[i]);
	free(D->map);
	mlx_destroy_image(D->window.m, D->window.img.imgg);
	mlx_destroy_window(D->window.m, D->window.w);
	exit(0);
}

int			exit_x(void *par)
{
	t_all temp;

	par = NULL;
	temp = storage(&temp, 0);
	clear_all(&temp);
	return (0);
}

void		exit_error(int err)
{
	if (err == 404)
		printf("File does not exist\n");
	else if (err == -1)
		printf("File is invalid\n\tDifferent length of input strings\n");
	else if (err == 1)
		printf("File is invalid\n\tNot boxed map\n");
	else if (err == 2)
		printf("File is invalid\n\tNo free space\n");
	exit(-1);
}
