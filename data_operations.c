/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:35:12 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:35:14 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_all		storage(t_all *data, int write)
{
	static t_all wnd;

	if (write == 1)
		wnd = *D;
	return (wnd);
}

void		init(t_all *data)
{
	D->window.img.imgg = mlx_new_image(D->window.m, W, H);
	D->window.img.D = mlx_get_data_addr(D->window.img.imgg,
		&D->window.img.bpp, &D->window.img.size_line,
		&D->window.img.byteorder);
	D->camera.dirx = -1;
	D->camera.diry = 0;
	D->camera.planex = 0;
	D->camera.planey = 0.7;
	D->camera.camx = 0;
	D->camera.mleftx = 0;
	D->camera.mlefty = 0.5;
	storage(D, 1);
}
