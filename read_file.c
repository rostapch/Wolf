/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:32:47 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:32:48 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		check_valid(int valid)
{
	if (valid == 1)
		exit_error(1);
	if (valid == 0)
		exit_error(2);
}

void		fill_map(t_all *data, char *file)
{
	int		j;
	int		i;
	int		ppp;

	D->map = ft_fill_int_tab(data->xy, file);
	ppp = 0;
	i = -1;
	while (++i < D->xy[1])
	{
		j = -1;
		while (++j < D->xy[0])
		{
			if (D->map[i][j] == 0 && (i == 0 || j == 0 ||
				i >= D->xy[1] - 1 || j >= D->xy[0] - 1))
				ppp = 1;
			if (D->map[i][j] == 0 && ppp == 0)
			{
				D->camera.posx = i + 0.2;
				D->camera.posy = j + 0.2;
				ppp = 2;
			}
		}
	}
	check_valid(ppp);
}

void		read_file(t_all *data, char *file)
{
	char	*line;
	int		fd;

	fd = open(file, O_RDONLY);
	D->xy[0] = 0;
	D->xy[1] = 0;
	if (fd == -1)
	{
		exit_error(404);
	}
	while (ft_get_next_line(fd, &line) > 0)
	{
		if (D->xy[1] == 0)
			D->xy[0] = ft_words(line);
		if (D->xy[0] != ft_words(line))
		{
			ft_strdel(&line);
			exit_error(-1);
		}
		ft_strdel(&line);
		D->xy[1]++;
	}
	ft_strdel(&line);
	fill_map(D, file);
}
