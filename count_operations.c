/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:28:23 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:28:25 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		init_all(t_all *data, int x)
{
	D->camera.camx = 2 * (double)x / (double)(W - 1) - 1;
	DRP.posx = D->camera.posx;
	DRP.posy = D->camera.posy;
	DRP.dirx = D->camera.dirx + D->camera.planex * D->camera.camx;
	DRP.diry = D->camera.diry + D->camera.planey * D->camera.camx;
	DCP.mapx = (int)DRP.posx;
	DCP.mapy = (int)DRP.posy;
	DCP.ddistx = sqrt(1 + (DRP.diry * DRP.diry) / (DRP.dirx * DRP.dirx));
	DCP.ddisty = sqrt(1 + (DRP.dirx * DRP.dirx) / (DRP.diry * DRP.diry));
	DCP.hit = 0;
}

void		ray_dir(t_all *data)
{
	if (DRP.dirx < 0)
	{
		DCP.stepx = -1;
		DCP.sdistx = (DRP.posx - DCP.mapx) * DCP.ddistx;
	}
	else
	{
		DCP.stepx = 1;
		DCP.sdistx = (DCP.mapx + 1.0 - DRP.posx) * DCP.ddistx;
	}
	if (DRP.diry < 0)
	{
		DCP.stepy = -1;
		DCP.sdisty = (DRP.posy - DCP.mapy) * DCP.ddisty;
	}
	else
	{
		DCP.stepy = 1;
		DCP.sdisty = (DCP.mapy + 1.0 - DRP.posy) * DCP.ddisty;
	}
}

void		hit_check(t_all *data)
{
	while (!DCP.hit)
	{
		if (DCP.sdistx < DCP.sdisty)
		{
			DCP.sdistx += DCP.ddistx;
			DCP.mapx += DCP.stepx;
			DCP.side = 0;
		}
		else
		{
			DCP.sdisty += DCP.ddisty;
			DCP.mapy += DCP.stepy;
			DCP.side = 1;
		}
		if (D->map[DCP.mapx][DCP.mapy] > 0)
			DCP.hit = 1;
	}
}

void		visible_line(t_all *data)
{
	if (DCP.side == 0)
		D->dd.pdist = (DCP.mapx - DRP.posx + ((1 - DCP.stepx) >> 1)) / DRP.dirx;
	else
		D->dd.pdist = (DCP.mapy - DRP.posy + ((1 - DCP.stepy) >> 1)) / DRP.diry;
	D->dd.height = (int)(H / D->dd.pdist);
	D->dd.dstart = (H >> 1) - (D->dd.height >> 1);
	if (D->dd.dstart < 0)
		D->dd.dstart = 0;
	D->dd.dend = (D->dd.height >> 1) + (H >> 1);
	if (D->dd.dend >= H)
		D->dd.dend = H - 1;
}

void		wall_color(t_all *data)
{
	int color;

	color = DCP.side == 1 ? 100 : 220;
	if (MAPELEM == 1)
		color = ft_rgb(color, 0, 0);
	else if (MAPELEM == 2)
		color = ft_rgb(0, color, 0);
	else if (MAPELEM == 3)
		color = ft_rgb(0, 0, color);
	else if (MAPELEM == 4)
		color = ft_rgb(142, 95, 56);
	else
		color = ft_rgb(color, color, 0);
	D->xclr.color = color;
}
