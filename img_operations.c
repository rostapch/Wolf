/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_operations.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:30:29 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:30:31 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		data_to_img(t_all *data, int x)
{
	pthread_t	thr[3];
	t_all		tmp[3];

	D->xclr.x = x;
	D->dd.lstart = 3 * (H >> 2);
	D->dd.lend = H;
	ft_memcpy(&tmp[0], D, sizeof(t_all));
	tmp[0].dd.lstart = 0;
	tmp[0].dd.lend = tmp[0].dd.lstart + (H >> 2);
	ft_memcpy(&tmp[1], D, sizeof(t_all));
	tmp[1].dd.lstart = (H >> 2);
	tmp[1].dd.lend = (H >> 1);
	ft_memcpy(&tmp[2], D, sizeof(t_all));
	tmp[2].dd.lstart = (H >> 1);
	tmp[2].dd.lend = 3 * (H >> 2);
	pthread_create(&(thr[0]), NULL, putpixel, &tmp[0]);
	pthread_create(&(thr[1]), NULL, putpixel, &tmp[1]);
	pthread_create(&(thr[2]), NULL, putpixel, &tmp[2]);
	putpixel(D);
	pthread_join(thr[0], NULL);
	pthread_join(thr[1], NULL);
	pthread_join(thr[2], NULL);
}

void		put_pixel_img(t_img *img, int x, int y, int c)
{
	if (0 <= x && x < W)
		if (0 <= y && y < H)
			*(int *)(img->D + (x + y * W) * img->bpp / 8) = c;
}

void		*putpixel(void *tmp)
{
	t_all	*data;
	int		i;

	D = tmp;
	i = D->dd.lstart - 1;
	while (++i < D->dd.lend)
	{
		if (i >= D->dd.dstart && i <= D->dd.dend)
			put_pixel_img(&D->window.img, D->xclr.x, i, D->xclr.color);
		else if (i < (H >> 1))
			put_pixel_img(&D->window.img, D->xclr.x, i, SKY);
		else
			put_pixel_img(&D->window.img, D->xclr.x, i, FLR);
	}
	return (tmp);
}

void		draw_cross(t_all *data)
{
	mlx_pixel_put(D->window.m, D->window.w, W / 2 - 1, (H >> 1), SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2 - 2, (H >> 1), SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2 - 3, (H >> 1), SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2 + 1, (H >> 1), SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2 + 2, (H >> 1), SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2 + 3, (H >> 1), SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2, (H >> 1) - 1, SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2, (H >> 1) - 2, SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2, (H >> 1) - 3, SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2, (H >> 1) + 1, SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2, (H >> 1) + 2, SCP);
	mlx_pixel_put(D->window.m, D->window.w, W / 2, (H >> 1) + 3, SCP);
}
