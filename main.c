/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/12 16:04:50 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/12 16:04:52 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		drawpic(t_all *data)
{
	int x;

	x = -1;
	while (++x < W)
	{
		init_all(D, x);
		ray_dir(D);
		hit_check(D);
		visible_line(D);
		wall_color(D);
		data_to_img(D, x);
	}
	storage(D, 1);
	mlx_put_image_to_window(D->window.m, D->window.w, D->window.img.imgg, 0, 0);
	ft_bzero(D->window.img.D, H * W * D->window.img.bpp / 8);
	draw_cross(D);
}

int			main(int argc, char **argv)
{
	t_all	data;

	if (argc == 2)
	{
		D.window.m = mlx_init();
		D.window.w = mlx_new_window(D.window.m, W, H, "wolf");
		read_file(&D, argv[1]);
		init(&D);
		drawpic(&D);
		mlx_hook(D.window.w, MotionNotify, PointerMotionMask, &mouse_move,
			None);
		mlx_hook(D.window.w, 17, 1L << 17, exit_x, D.window.m);
		mlx_hook(D.window.w, KeyPress, KeyPressMask, key_press, &D.window);
		mlx_loop(D.window.m);
	}
	return (0);
}
