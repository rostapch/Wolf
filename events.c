/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:31:53 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:31:54 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int			mouse_move(int x, int y, void *param)
{
	double	tmp;
	t_all	temp;

	tmp = y;
	(void)param;
	temp = storage(&temp, 0);
	if (abs(x) > 0)
	{
		tmp = TCAM.dirx;
		TCAM.dirx = TCAM.dirx * COST - TCAM.diry * SINT;
		TCAM.diry = tmp * SINT + TCAM.diry * COST;
		tmp = TCAM.planex;
		TCAM.planex = TCAM.planex * COST - TCAM.planey * SINT;
		TCAM.planey = tmp * SINT + TCAM.planey * COST;
		tmp = TCAM.mleftx;
		TCAM.mleftx = TCAM.mleftx * COST - TCAM.mlefty * SINT;
		TCAM.mlefty = tmp * SINT + TCAM.mlefty * COST;
	}
	mlx_clear_window(temp.window.m, temp.window.w);
	drawpic(&temp);
	return (1);
}

int			key_press1(int key, t_all *temp)
{
	if (KEYISD)
	{
		if (!TMPMP[(int)(TCAMP.posx - TCAMP.dirx * MS)][(int)(TCAMP.posy)])
			TCAMP.posx -= TCAMP.dirx * MS;
		if (!TMPMP[(int)(TCAMP.posx)][(int)(TCAMP.posy - TCAMP.diry * MS)])
			TCAMP.posy -= TCAMP.diry * MS;
	}
	if (KEYISL)
	{
		if (!TMPMP[(int)(TCAMP.posx - TCAMP.mleftx * MS)][(int)(TCAMP.posy)])
			TCAMP.posx -= TCAMP.mleftx * MS;
		if (!TMPMP[(int)(TCAMP.posx)][(int)(TCAMP.posy - TCAMP.mlefty * MS)])
			TCAMP.posy -= TCAMP.mlefty * MS;
	}
	if (KEYISR)
	{
		if (!TMPMP[(int)(TCAMP.posx + TCAMP.mleftx * MS)][(int)(TCAMP.posy)])
			TCAMP.posx += TCAMP.mleftx * MS;
		if (!TMPMP[(int)(TCAMP.posx)][(int)(TCAMP.posy + TCAMP.mlefty * MS)])
			TCAMP.posy += TCAMP.mlefty * MS;
	}
	return (1);
}

int			key_press(int key)
{
	t_all temp;

	temp = storage(&temp, 0);
	key_press1(key, &temp);
	if (KEYISU)
	{
		if (!temp.map[(int)(TCAM.posx + TCAM.dirx * MS)][(int)(TCAM.posy)])
			TCAM.posx += TCAM.dirx * MS;
		if (!temp.map[(int)(TCAM.posx)][(int)(TCAM.posy + TCAM.diry * MS)])
			TCAM.posy += TCAM.diry * MS;
	}
	if (key == 53)
		clear_all(&temp);
	if (KEYISU || KEYISD || KEYISL || KEYISR)
	{
		mlx_clear_window(temp.window.m, temp.window.w);
		drawpic(&temp);
	}
	return (0);
}
