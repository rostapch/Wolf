# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/25 21:36:34 by rostapch          #+#    #+#              #
#    Updated: 2017/08/25 21:36:36 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

NAME = wolf3d

FRAMEFLAGS = -framework OpenGL -framework AppKit

INCLUDE_LIB = -L libft -l ft -L minilibx_macos_2016 -l mlx

HEADERS =  -I . -I libft

SRC_C =	count_operations.c\
		data_operations.c\
		img_operations.c\
		other_operations.c\
		events.c\
		exit.c\
		main.c\
		read_file.c

SRC_O =	count_operations.o\
		data_operations.o\
		img_operations.o\
		other_operations.o\
		events.o\
		exit.o\
		main.o\
		read_file.o
		

OBJ = $(SRC_C:.c=.o)

all: libx lib $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(SRC_O) $(FRAMEFLAGS) $(INCLUDE_LIB) $(HEADERS)

%.o: %.c
	$(CC) -c $(CFLAGS) $(HEADERS)  -o $@ $<

lib:
	make -C libft -f Makefile

lib_re:
	make re -C libft -f Makefile

lib_clean:
	make clean -C libft -f Makefile

lib_fclean:
	make fclean -C libft -f Makefile



libx:
	make -C minilibx_macos_2016 -f Makefile

libx_clean:
	make clean -C minilibx_macos_2016 -f Makefile

libx_re:
	make re -C minilibx_macos_2016 -f Makefile



clean_fdf:
	/bin/rm -f $(SRC_O)

fclean_fdf: clean_fdf
	/bin/rm -f $(NAME)

clean: lib_clean libx_clean
	/bin/rm -f $(SRC_O)



fclean: lib_fclean clean
	/bin/rm -f $(NAME)

re: fclean_fdf lib_re libx_re $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(SRC_O) $(FRAMEFLAGS) $(INCLUDE_LIB) $(HEADERS)
#all:
#	gcc *.c -framework OpenGL -framework AppKit -I /opt/X11/include -I . -L libft -l ft -L minilibx_macos_2016 -l mlx -o wolf3d
