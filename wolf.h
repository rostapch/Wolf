/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/12 17:53:03 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/12 17:53:05 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <pthread.h>
# include "math.h"
# include "libft/libft.h"
# include "libft/get_next_line.h"
# include "/opt/X11/include/X11/X.h"
# include "minilibx_macos_2016/mlx.h"

# define K_U		126
# define K_L		123
# define K_D		125
# define K_R		124

# define KA_U		13
# define KA_L		0
# define KA_D		1
# define KA_R		2

# define H			480
# define W			640

# define MS			0.2
# define RS			0.00022

# define SKY		0x6eddde
# define FLR		0x3e2820
# define SCP 		0x00ffff

# define MAXTS fmin(1100, (x * x))
# define COST (x < 0 ? cos(RS * MAXTS) : cos(-RS * MAXTS))
# define SINT (x < 0 ? sin(RS * MAXTS) : sin(-RS * MAXTS))

# define TCAM temp.camera
# define TCAMP temp->camera
# define TMPMP temp->map
# define DCP data->counts
# define DRP data->ray
# define D data

# define MAPELEM data->map[DCP.mapx][DCP.mapy]

# define KEYISU key == K_U || key == KA_U
# define KEYISD key == K_D || key == KA_D
# define KEYISL key == K_L || key == KA_L
# define KEYISR key == K_R || key == KA_R

typedef struct		s_xc
{
	int				x;
	int				color;
}					t_xc;

typedef struct		s_img
{
	void			*imgg;
	int				size_line;
	int				bpp;
	int				width;
	int				height;
	int				type;
	int				format;
	int				byteorder;
	char			*data;
}					t_img;

typedef struct		s_wind
{
	t_img			img;
	void			*m;
	void			*w;
}					t_wind;

typedef struct		s_raypos
{
	double			posx;
	double			posy;
	double			dirx;
	double			diry;
}					t_raypos;

typedef struct		s_campos
{
	double			posx;
	double			posy;
	double			dirx;
	double			diry;
	double			planex;
	double			planey;
	double			camx;
	double			mleftx;
	double			mlefty;
}					t_campos;

typedef struct		s_counts
{
	int				mapx;
	int				mapy;
	double			sdistx;
	double			sdisty;
	double			ddistx;
	double			ddisty;
	int				stepx;
	int				stepy;
	int				hit;
	int				side;
}					t_counts;

typedef struct		s_drawdata
{
	double			pdist;
	int				height;
	int				dstart;
	int				dend;
	int				lstart;
	int				lend;
}					t_drawdata;

typedef struct		s_all
{
	int				**map;
	int				xy[2];
	t_wind			window;
	t_campos		camera;
	t_raypos		ray;
	t_counts		counts;
	t_drawdata		dd;
	t_xc			xclr;
}					t_all;

void				init(t_all *data);

int					mouse_move(int x, int y, void *param);
int					key_press1(int key, t_all *temp);
int					key_press(int key);

void				put_pixel_img(t_img *img, int x, int y, int c);
void				*putpixel(void *tmp);

t_all				storage(t_all *data, int write);

void				clear_all(t_all *data);
int					exit_x(void *par);

void				ft_fill(char *str, int *tab_row);
int					ft_words(char *str);
int					**ft_fill_int_tab(int xy[2], char *file);

void				init_all(t_all *data, int x);
void				ray_dir(t_all *data);
void				hit_check(t_all *data);
void				visible_line(t_all *data);
void				wall_color(t_all *data);
void				data_to_img(t_all *data, int x);
void				draw_cross(t_all *data);
void				drawpic(t_all *data);

void				read_file(t_all *data, char *file);
void				fill_map(t_all *data, char *file);
void				check_valid(int valid);
void				exit_error(int err);

#endif
