/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other_operations.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 21:34:02 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/25 21:34:04 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int			ft_words(char *str)
{
	int i;
	int wrd;

	i = 0;
	wrd = 0;
	while ((size_t)i < ft_strlen(str))
	{
		if (str[i] > 32 && str[i + 1] <= 32)
			wrd++;
		i++;
	}
	return (wrd);
}

void		ft_fill(char *str, int *tab_row)
{
	char	**arr;
	int		i;
	int		j;

	i = -1;
	j = -1;
	arr = ft_strsplit(str, ' ');
	while (arr[++i])
		if (ft_strlen(arr[i]) > 0)
			tab_row[++j] = ft_atoi(arr[i]);
	while (i--)
		free(arr[i]);
	free(arr);
}

int			**ft_fill_int_tab(int xy[2], char *file)
{
	char	*line;
	int		**z;
	int		i;
	int		fd;

	z = (int **)malloc(sizeof(int *) * xy[1]);
	if (xy[1] <= 0 || xy[0] <= 0)
		exit(0);
	i = -1;
	while (++i < xy[1])
		z[i] = (int *)malloc(sizeof(int) * xy[0]);
	fd = open(file, O_RDONLY);
	i = 0;
	while (ft_get_next_line(fd, &line) > 0)
	{
		ft_fill(line, z[i]);
		ft_strdel(&line);
		i++;
	}
	ft_strdel(&line);
	return (z);
}
